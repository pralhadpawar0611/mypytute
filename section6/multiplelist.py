"""
Iterating multiple list at same time
"""

l1 = [1,2,3]
l2 = [6,7,8,20,30,40]

for a ,b in zip(l1,l2):
    print(a)
    print(b)

print()

for x,y in zip(l1,l2):
    if x > y:
        print(x)
    else:
        print(y)


print()
for x,y in zip(l1,l2):
    if y > x :
        print(y)
    else:
        print(x)