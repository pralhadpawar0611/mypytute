"""
executes statement rapidly
Conditions are used to stop the execution of loops
Iterable items are Strings, List , Table , Dictionary
"""
# String for Loop
my_string= 'abcabc'


for c in my_string:
    print(c)


for s in my_string:
    print(s, end=' ')

# end is positional variable that's available and use to replace space instead of new line
print()
print("-"*20)
for d in my_string:
    if d=='a':
        print('A',end=' ')
    else:
        print(d, end=' ')


# List for Loop
print()
cars =['bmw','benz','honda']
for car in cars:
    print(car)

print()
nums =[1,2,3]
for n in nums:
    print(10*n)

# Tuple for Loop
print()
names =('Roy','Bela','Alex')
for name in names:
    print(name)

# Dictionary for Loop
print()
d= {'one':1 ,'two':2 ,'three':3}
for k in d:
    print(k)
    print(k+" "+str(d[k]))


print()
for l,m in d.items():
    print(l)
    print(m)

