"""
Built-in function
Creates a sequence of numbers but does not save them in memory
Very useful for generating numbers
"""


print(range(1,12))
print(list(range(1,12)))
# To see generated numbers we need cast the range in List and it excludes of last range number

print(list(range(5)))  # If starting point is not mentioned of range then it starts range from 0 by default


a = range(0, 20, 6)
print(a)
# 6 is at step position , which used to print numbers in skip 6 numbers like 6,12,18

print(type(a))
print(list(a))


for num in range(1, 4):
    print(num)