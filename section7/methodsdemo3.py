"""
Positional Parameters
They are like optional parameters
And can be assigned a default value, if no value is provided from outside
"""


def sum_nums(n1=5, n2=4):
    """
    Get sum of two numbers
    :param n1:
    :param n2:
    :return:
    """
    return n1 + n2

print(sum_nums()) # as we provide values for parameters in menthod itself so need to pass parameter while calling it.
print(sum_nums(11,4)) # as we provide values of parameters for method while calling it, It will override the method level values.
print(sum_nums(n2=8)) #  as we provide value for only one parameter thne it will take value of second parameter from method level.
print(sum_nums(n2=8,n1=0)) # position of parameters is also not matter while passing it.



sum1 = sum_nums(4, n2=12)
print(sum1) # As 4 is directly added and 12 is value pass through n2 parameter then it will override method level values


def sum_nums(n1, n2=4):
    return n1 + n2

sum1 = sum_nums(7, n2=12)
print(sum1)